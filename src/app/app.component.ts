import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Http, Response, Headers} from '@angular/http';
import { Observable} from "rxjs/Rx";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
        
    constructor (private http:Http) {}

    private baseUrl: string = 'http://localhost:8080/RestaurantTableReservation_Services/';
    public submitted: boolean;
    restaurantsearch: FormGroup;
    userLogin: FormGroup;
    reserveTableRequest : FormGroup;
    collectionsDetails: ColletionDetails;
    restaurantsDetails: RestaurantDetails;
    loginResponse : LoginResponse;
    reservation : Reservation;
    public button: string;
    public restaurantID : string;
    @ViewChild('closeBtn') closeBtn: ElementRef;
    @ViewChild('closeBtn2') reserveModal: ElementRef;
    @ViewChild('modalSuccess') successModal: ElementRef;

    ngOnInit() {
        this.restaurantsearch = new FormGroup({
            cityName: new FormControl(''),
            restaurantName: new FormControl('')
        });
        this.userLogin = new FormGroup({
            userName : new FormControl(''),
            password: new FormControl('') 
        });
        this.reserveTableRequest = new FormGroup({
            reservationDate : new FormControl(''),
            fromTime : new FormControl(''),
            toTime : new FormControl(''),
            restaurantID : new FormControl(''),
            status : new FormControl(''),
            userName : new FormControl('')
        });
    }

    onSubmit({value, valid}: { value: Restaurantsearch, valid: boolean }) {
        console.log(value);
        //this.collectionsDetails = COLLECTION_LIST;
        this.getAllCollections(value.cityName)
            .subscribe(
                collectionsDetails => this.collectionsDetails = collectionsDetails,
                err => {
                    console.log(err);    
                }
            );
        this.button = "View";
    }

    viewCollection(collectionID: number, entityID: number) {
        console.log("Collection id for view:" + collectionID + " and entity ID is:" +  entityID);
        this.collectionsDetails = null;
        //this.restaurantsDetails = RESTAURANT_LIST;
        
        this.getCollectionsRestaurantDetails(collectionID, entityID)
            .subscribe(
                restaurantsDetails => this.restaurantsDetails = restaurantsDetails,
                err => {
                    console.log(err);
                }
            );
    }
    
    setRestauantID(restaurantID : string) {
        console.log("Restaurant ID: " + restaurantID);
        this.restaurantID = restaurantID;
    }
    
    reserveRestaurantTable({value, valid}: { value: ReserveTableRequest, valid: boolean }) {
        value.restaurantID = this.restaurantID;
        value.userName = "Akriti";
        value.status = "Active";
        console.log(value);
        this.makeReservation(value)
        .subscribe(
                reservation => this.reservation = reservation,
                err => {
                    console.log(err);
                }
            );
        this.reserveModal.nativeElement.click();
    }
    
    makeReservation(value : ReserveTableRequest) : Observable<Reservation> {
        let bodyString = JSON.stringify(value); 
        return this.http.post(this.baseUrl + 'reservation/book', bodyString, headerObj)
        .map(this.mapReservation);
    }
    
    mapReservation(response:Response): Reservation {
        return response.json();
    }
    
    validateUser({value, valid}: { value: UserLogin, valid: boolean }) {
        console.log(value);
        this.closeBtn.nativeElement.click();
        
        this.validateCredentials(value.userName, value.password)
            .subscribe(
                loginResponse => this.loginResponse = loginResponse,
                err => {
                    console.log(err);    
                }
            );
    }
    
    validateCredentials(userName : string, password : string) {
        return this.http.post(this.baseUrl + 'oauth/token?grant_type=password&username=' + userName + '&password=' + password, headerObj)
        .map(this.mapLoginResponse);
    }
    
    mapLoginResponse(response:Response) : LoginResponse {
        return response.json();
    }
    
    getAllCollections(location: string): Observable<ColletionDetails> {
        return this.http.get(this.baseUrl + 'restaurant/locations?query=' + location)
        .map(this.mapCollections);
    }
    
    mapCollections(response:Response): ColletionDetails {
        return response.json();
    }
    
    getCollectionsRestaurantDetails(collectionID: number, entityID: number): Observable<RestaurantDetails> {
        return this.http.get(this.baseUrl + 'restaurant/search?entity_id=' + entityID + '&entity_type=city&collection_id=' + collectionID)
        .map(this.mapResaturantCollections);
    }
    
    mapResaturantCollections(response:Response): RestaurantDetails {
        return response.json();
    }
}

const headerDict = {
  'Content-Type': 'application/json',
  'Accept': 'application/json',
  'Access-Control-Allow-Headers': 'Content-Type',
  'Access-Control-Allow-Origin' : 'http://localhost:4200',
  'Authorization' : 'Basic bXktdHJ1c3RlZC1jbGllbnQ6c2VjcmV0'
}

const headerObj = {                                                                                                                                                                                 
  headers: new Headers(headerDict), 
};

export interface Restaurantsearch {
    cityName: string;
    restaurantName: string;
}

export interface Reservation {
    creationDate : string;
    reservationDate : string;
    fromTime : string;
    toTime : string;
    restaurantID : string;
    status : string;
    userName : string;
    reservationIdentifier : string;    
}

export interface ReserveTableRequest {
    reservationDate : string;
    fromTime : string;
    toTime : string;
    restaurantID : string;
    status : string;
    userName : string;
}

export interface LoginResponse {
    access_token : string;
    token_type : string;
    refresh_token : string;
    expires_in : string;
    scope : string;
    error : string;
    error_description : string;   
}

export interface UserLogin {
    userName : string;
    password: string;   
}

export interface ColletionDetails {
    collections: Collections[];
    has_more: string;
    entity_id: number;
    has_total: string;
    display_text: string;
    share_url: string;
}

export interface Collections {
    collection: Collection;
}

export interface Collection {
    collection_id: number;
    res_count: number;
    image_url: string;
    url: string;
    title: string;
    description: string;
    share_url: string;
}

export interface Location {
    address: string;
    locality: string;
    city: string;
    city_id: number;
    latitude: string;
    longitude: string;
    zipcode: string;
    country_id: number;
    locality_verbose: string;
}

export interface UserRating {
    aggregate_rating: string;
    rating_text: string;
    rating_color: string;
    votes: string;
}

export interface Restaurant {
    id: string;
    name: string;
    location: Location;
    cuisines: string;
    average_cost_for_two: number;
    currency: string;
    user_rating: UserRating;
    thumb: string;
    photos_url: string;
    menu_url: string;
    has_table_booking: number;
}

export interface Restaurants {
    restaurant: Restaurant;
}

export interface RestaurantDetails {
    results_found: number;
    results_shown: number;
    restaurants: Restaurants[];
}

